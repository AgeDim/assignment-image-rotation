#ifndef PADDING_H
#define PADDING_H

#include <stdint.h>

uint8_t get_padding(uint64_t width);

#endif //PADDING_H
