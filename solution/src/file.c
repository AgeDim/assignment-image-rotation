#include "../include/bmp.h"
#include "../include/file.h"
#include <string.h>

const char* messages[] = {
        [OK] = "OK.\n",
        [F_OPEN_ERROR] = "File cannot be opened.\n",
        [F_R_ERROR] = "File cannot be read.\n",
        [H_R_ERROR] = "Error occurred while reading header from file.\n",
        [C_R_ERROR] = "Error occurred while reading content from file.\n",
        [H_WR_ERROR] = "Error occurred while writing header to file.\n",
        [C_WR_ERROR] = "Error occurred while writing content to file.\n",
        [F_WR_ERROR] = "Cannot write to file.\n",
        [F_CLOSE_ERROR] = "File cannot be closed.\n",
        [F_FORMAT_ERROR] = "Cannot resolve file format.\n"
};

void print_status_code(enum status_code status) {
    printf("%s", messages[status]);
}

bool f_open(const char* file_name, const char* mode, struct image* image) {
    FILE* file = fopen(file_name, mode);
    if(file == NULL) {
        print_status_code(F_OPEN_ERROR);
        return false;
    } else {
        if (strcmp(mode, "rb") == 0) {
            print_status_code(from_bmp(file, image));
            f_close(file);
            return true;
        }
        if (strcmp(mode, "wb") == 0) {
            struct image output_image = rotating(*image);
            print_status_code(to_bmp(file, &output_image));
            free_image(*image);
            free_image(output_image);
            f_close(file);
            return true;
        }
        f_close(file);
        return false;
    }
}

void f_close(FILE* file) {
    if (fclose(file) != 0) {
        print_status_code(F_CLOSE_ERROR);
    }
}
